# V42 Client/Server/Master Infrastructure

[`V42-csmi`](.) implements a 3-tier framework of remote clients, distributed servers, and a master to orchestrate the whole environment. Whatever purpose this framework is going to be used for is only limited by your creativity. `V42-csmi` implements a solid and secure infrastructure, allowing you to concentrate on the application.

- [`Clients`](client) to be described ...

- [`Servers`](server) to be described ...

- [`Master`](master)  to be described ...

## License
This project is [licensed](LICENSE.md) under the **GNU General Public License v3.0.** [*Learn more*](https://choosealicense.com/licenses/gpl-3.0/)
