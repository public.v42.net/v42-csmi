package main

import (
    "crypto/ed25519"
	"crypto/x509"
	"crypto/rand"
	"encoding/base64"
    "encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
)

type ClientKey struct {
	Public, Private, Random string
}

func main() {
    pubKey, privKey, _ := ed25519.GenerateKey(rand.Reader)
    pubBytes, _ := x509.MarshalPKIXPublicKey(pubKey)
    pubString := base64.RawURLEncoding.EncodeToString(pubBytes)
    privBytes, _ := x509.MarshalPKCS8PrivateKey(privKey)
    privString := base64.RawURLEncoding.EncodeToString(privBytes)
    randInt, _ := rand.Int(rand.Reader, big.NewInt(999))
    randString := fmt.Sprintf("%03d", randInt)
    Key := ClientKey { Public: pubString, Private: privString, Random: randString }
    jsonBytes, _ := json.MarshalIndent(Key, "", "  ")
    _ = ioutil.WriteFile("clientid.key", jsonBytes, 0644)
    jsonString := string(jsonBytes)
    fmt.Println(jsonString)
}
