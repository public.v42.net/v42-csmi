package main

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"os"
	"strings"
)

func main() {
	args := append(os.Args, "http://example.com/")
	fmt.Println("GET", args[1])
	resp, err := http.Get(args[1])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	header := strings.Split(string(dump), "\r\n\r\n")
	fmt.Println(header[0])
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	resp.Body.Close()
	err = os.WriteFile("htclient.dat", body, 0644)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	os.Exit(0)
}
