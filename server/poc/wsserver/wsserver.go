package main

import (
    "fmt"
	"log"
	"net/http"
	"os"
	"time"
	
	"github.com/gorilla/websocket"
)

func main() {
    https := false
    _, err := os.Stat("wsserver.crt")
    if err == nil {
        _, err = os.Stat("wsserver.key")
        if err == nil {
            https = true
        }
    }
    upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	wshandler := func(w http.ResponseWriter, r *http.Request) {
		websocket, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}
		log.Println("Websocket Connected!")
		listen(websocket)
	}
	http.HandleFunc("/", wshandler)
	if https {
        log.Fatal(http.ListenAndServeTLS(":8443", "wsserver.crt", "wsserver.key", nil))
	} else {
        log.Fatal(http.ListenAndServe(":8080", nil))
    }
	os.Exit(0)
}
func listen(conn *websocket.Conn) {
	for {
		// read a message
		messageType, messageContent, err := conn.ReadMessage()
		timeReceive := time.Now()
		if err != nil {
			log.Println(err)
			return
		}

		// print out that message
		fmt.Println(string(messageContent))

		// reponse message
		messageResponse := fmt.Sprintf("Your message is: %s. Time received : %v", messageContent, timeReceive)

		if err := conn.WriteMessage(messageType, []byte(messageResponse)); err != nil {
			log.Println(err)
			return
		}

	}
}
