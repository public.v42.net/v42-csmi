package main

import (
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
    https := false
    _, err := os.Stat("htserver.crt")
    if err == nil {
        _, err = os.Stat("htserver.key")
        if err == nil {
            https = true
        }
    }

	helloHandler := func(w http.ResponseWriter, req *http.Request) {
        switch req.TLS.Version {
            case 0x0300:
                w.Header().Add("tls", "SSLv3.0")
            case 0x0301:
                w.Header().Add("tls", "TLSv1.0")
            case 0x0302:
                w.Header().Add("tls", "TLSv1.1")
            case 0x0303:
                w.Header().Add("tls", "TLSv1.2")
            case 0x0304:
                w.Header().Add("tls", "TLSv1.3")
        }		
		io.WriteString(w, "Hello World\n")
	}

	http.HandleFunc("/", helloHandler)
	if https {
        log.Fatal(http.ListenAndServeTLS(":8443", "htserver.crt", "htserver.key", nil))
	} else {
        log.Fatal(http.ListenAndServe(":8080", nil))
    }


	/* 	args := append(os.Args, "http://example.com/")
	   	fmt.Println("GET", args[1])
	   	resp, err := http.Get(args[1])
	   	if err != nil {
	   		fmt.Println(err)
	   		os.Exit(1)
	   	}
	   	dump, err := httputil.DumpResponse(resp, true)
	   	if err != nil {
	   		fmt.Println(err)
	   		os.Exit(1)
	   	}
	   	header := strings.Split(string(dump), "\r\n\r\n")
	   	fmt.Println(header[0])
	   	body, err := io.ReadAll(resp.Body)
	   	if err != nil {
	   		fmt.Println(err)
	   		os.Exit(1)
	   	}
	   	resp.Body.Close()
	   	err = os.WriteFile("htclient.dat", body, 0644)
	   	if err != nil {
	   		fmt.Println(err)
	   		os.Exit(1)
	   	} */

	os.Exit(0)
}
